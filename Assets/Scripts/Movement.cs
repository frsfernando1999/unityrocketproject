using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class Movement : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private float rocketSpeed = 50f;
    [SerializeField] private float rotationSpeed = 10f;
    [SerializeField] private int frameRate = -1;
    [SerializeField] private AudioClip engineSound;
    [SerializeField] private ParticleSystem mainBooster;
    [SerializeField] private ParticleSystem mainBooster2;
    [SerializeField] private ParticleSystem leftBooster;
    [SerializeField] private ParticleSystem rightBooster;

    private Rigidbody _rigidbody;
    private AudioSource _audioSource;

    private bool _leftPressed;
    private bool _rightPressed;
    private bool _spacePressed;
    private bool _bFlying;

    void Start()
    {
        Application.targetFrameRate = frameRate;
        _rigidbody = GetComponent<Rigidbody>();
        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        ProcessInput();
    }

    //Called every 0.02s/50 frames (Adjustable at the project settings on the Time->Fixed TimeStep)
    private void FixedUpdate()
    {
        RocketPhysicsAndParticles();
    }

    public void SetAudioSound(bool mute)
    {
        if (mute)
        {
            _audioSource.Stop();
        }
        else
        {
            _audioSource.Play();
        }
    }

    private void ProcessInput()
    {
        _leftPressed = Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftArrow);
        _rightPressed = Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.RightArrow);
        _spacePressed = Input.GetKey(KeyCode.Space);
        if (Input.GetKeyDown(KeyCode.Escape)) QuitGame();
    }

    private void QuitGame()
    {
        Application.Quit();
    }

    private void RocketPhysicsAndParticles()
    {
        Vector3 rotateDirection = Vector3.forward * rotationSpeed;

        if (_spacePressed)
        {
            BoostRocket();
            PlayParticles(mainBooster);
            PlayParticles(mainBooster2);
        }
        else
        {
            _audioSource.Stop();
            mainBooster.Stop();
            mainBooster2.Stop();
        }

        if (_leftPressed)
        {
            PlayParticles(rightBooster);
            RotateRocket(-rotateDirection);
        }
        else
        {
            rightBooster.Stop();
        }

        if (_rightPressed)
        {
            PlayParticles(leftBooster);
            RotateRocket(rotateDirection);
        }
        else
        {
            leftBooster.Stop();
        }
    }

    private void BoostRocket()
    {
        _rigidbody.AddRelativeForce(Vector3.up * rocketSpeed);
        if (!_audioSource.isPlaying)
        {
            _audioSource.PlayOneShot(engineSound);
        }
    }

    private void RotateRocket(Vector3 rotateDirection)
    {
        _rigidbody.freezeRotation = true; // Freeze collision rotation to manually rotate
        transform.Rotate(rotateDirection);
        _rigidbody.freezeRotation = false; // Unfreeze it right after rotating
    }

    void PlayParticles(ParticleSystem particles)
    {
        if (!particles.isPlaying)
        {
            particles.Play();
        }
    }
}