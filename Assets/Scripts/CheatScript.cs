using UnityEngine;
using UnityEngine.SceneManagement;

public class CheatScript : MonoBehaviour
{
    private bool collisionsOn = true;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L)) SkipLevel();
        if (Input.GetKeyDown(KeyCode.C)) CollisionCheat();
    }

    private void CollisionCheat()
    {
        collisionsOn = !collisionsOn;
        ToggleCollisions();
    }

    private void ToggleCollisions()
    {
        GameObject[] obstacles = GameObject.FindGameObjectsWithTag("Obstacle");
        GameObject[] walls = GameObject.FindGameObjectsWithTag("Wall");

        foreach (var obstacle in obstacles)
        {
            obstacle.GetComponent<Collider>().enabled = collisionsOn;
        }

        foreach (var wall in walls)
        {
            wall.GetComponent<Collider>().enabled = collisionsOn;
        }
    }

    private void SkipLevel()
    {
        int nextLevel = SceneManager.GetActiveScene().buildIndex + 1;
        if (nextLevel == SceneManager.sceneCountInBuildSettings)
        {
            nextLevel = 0;
        }

        SceneManager.LoadScene(nextLevel);
    }
}