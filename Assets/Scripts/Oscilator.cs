using UnityEngine;

public class Oscilator : MonoBehaviour
{
    private Vector3 _startingPosition;
    [SerializeField] private Vector3 movementVector;
    [SerializeField] [Range(0, 1)] private float movementFactor;
    [SerializeField] private float period = 2f;

    // Start is called before the first frame update
    void Start()
    {
        _startingPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (period <= Mathf.Epsilon) return;
        // calculations for a sine wave | Alternatively look into Vector3.Lerp
        float cycles = Time.time / period;

        const float tau = Mathf.PI * 2;
        float rawSineWave = Mathf.Sin(cycles * tau);

        movementFactor = (rawSineWave + 1f) * 0.5f;

        Vector3 offset = movementVector * movementFactor;
        transform.position = _startingPosition + offset;
    }
}