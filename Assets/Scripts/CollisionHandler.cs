using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    private bool _isTransitioning;

    [SerializeField] private float loadLevelDelay = 2f;
    [SerializeField] private AudioClip impactSound;
    [SerializeField] private AudioClip landSound;
    
    [SerializeField] private ParticleSystem impactParticles;
    [SerializeField] private ParticleSystem landParticles;
    
    private AudioSource _audioSource;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Obstacle":
            case "Wall":
                HandleDeath();
                break;
            case "LandingPad":
                HandleProgression();
                break;
        }
    }

    private void HandleProgression()
    {
        if (_isTransitioning) return;
        _isTransitioning = true;
        GetComponent<Movement>().enabled = false;
        PlayCollisionSound(landSound);
        PlayCollisionParticles(landParticles);
        Invoke(nameof(LoadNextLevel), loadLevelDelay);
    }
    
    private void LoadNextLevel()
    {
        int nextLevel = SceneManager.GetActiveScene().buildIndex + 1;
        if (nextLevel == SceneManager.sceneCountInBuildSettings)
        {
            nextLevel = 0;
        }

        SceneManager.LoadScene(nextLevel);
    }

    private void HandleDeath()
    {
        if (_isTransitioning) return;
        _isTransitioning = true;
        GetComponent<Movement>().enabled = false;
        PlayCollisionSound(impactSound);
        PlayCollisionParticles(impactParticles);
        Invoke(nameof(ReloadLevel), loadLevelDelay);
    }

    private void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void PlayCollisionSound([CanBeNull] AudioClip impactAudio)
    {
        GetComponent<Movement>().SetAudioSound(true);
        _audioSource.PlayOneShot(impactAudio);
    }
    private void PlayCollisionParticles(ParticleSystem particles)
    {
        particles.Play();
    }
}